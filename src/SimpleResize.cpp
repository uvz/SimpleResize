//  Simple (faster) resize for avisynth
//	Copyright (C) 2002 Tom Barry
//
//	This program is free software; you can rrdistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//  
//  Also, this program is "Philanthropy-Ware".  That is, if you like it and 
//  feel the need to reward or inspire the author then please feel free (but
//  not obligated) to consider joining or donating to the Electronic Frontier
//  Foundation. This will help keep cyber space free of barbed wire and bullsh*t.  
//
//  See their web page at www.eff.org

// Changes:
// 
// Jan 2003 0.3.3.0 Avisynth 2.5 YV12 support, AvisynthPluginit2
// Feb 2002 0.3.0.0 Added InterlacedResize support
// Jan 2002 0.2.0.0 Some rather ineffectual P3 SSE optimizations
// Jan 2002 0.1.0.0 First release 

#include <malloc.h>
#include <cstring>
#include <cmath>

#include "avisynth.h"

class SimpleResize : public GenericVideoFilter
{
	int newwidth;
	int newheight;
	int oldwidth;
	int oldheight;
	double minslope;
	double hWarp;
	double vWarp;
	unsigned int* hControl;		// weighting masks, alternating dwords for Y & UV
								// 1 qword for mask, 1 dword for src offset, 1 unused dword
    unsigned int* hControlUV;
	unsigned int* vOffsets;		// Vertical offsets of the source lines we will use
	unsigned int* vWeights;		// weighting masks, alternating dwords for Y & UV
	unsigned int* vOffsetsUV;		// Vertical offsets of the source lines we will use
	unsigned int* vWeightsUV;		// weighting masks, alternating dwords for Y & UV
	unsigned int* vWorkY;		// weighting masks 0Y0Y 0Y0Y...
	unsigned int* vWorkUV;		// weighting masks UVUV UVUV...
	bool SSE2enabled;
	bool SSEMMXenabled;
	bool Interlaced;
	bool DoYV12;
	bool has_at_least_v8;

	//	void LoadFrame(int n,int offset, IScriptEnvironment* env);
	//void SimpleResize::InitTables();
	int __stdcall InitTables(void);
	int __stdcall InitTables_YV12(void);
	PVideoFrame __stdcall GetFrame(int inFrame, IScriptEnvironment* env);
	PVideoFrame __stdcall GetFrame_YUY2(int n, IScriptEnvironment* env,
		PVideoFrame src, PVideoFrame dst, int Planar_Type, BYTE* dstp);
	PVideoFrame __stdcall GetFrame_YV12(int n, IScriptEnvironment* env,
		PVideoFrame src, PVideoFrame dst, int Planar_Type, BYTE* dstp);
	int __stdcall Fieldcopy(void* dest, const void* src, size_t count,
		int rows, int dst_pitch, int src_pitch);
	int __stdcall FieldComp(void* dest, const void* src, size_t count,
		int rows, int dst_pitch, int src_pitch);

	double __stdcall WarpFactor(double z, double wFact);

public:
	SimpleResize(PClip _child, int _width, int _height,
		double _hWarp, double _vWarp, bool _Interlaced, IScriptEnvironment* env)
		: GenericVideoFilter(_child)
	{
		has_at_least_v8 = true;
		try { env->CheckVersion(8); } catch (const AvisynthError&) { has_at_least_v8 = false; }

		minslope = 0.30;			// don't overstretch
		oldwidth = vi.width;
		oldheight = vi.height;
		newwidth = _width;
		newheight = _height;
		vi.width = newwidth;
		vi.height = newheight;
		hWarp = _hWarp;		// 1.15 remember I used hw=1.15, vw=.95 for sample
		vWarp = _vWarp;			// .95
		Interlaced = _Interlaced;
		SSE2enabled = (env->GetCPUFlags() & CPUF_SSE2) != 0;
		SSEMMXenabled = (env->GetCPUFlags() & CPUF_INTEGER_SSE) != 0;

		if (vi.IsYUY2())
		{
			DoYV12 = false;
		}
		else if (vi.IsYV12())
		{
			if (Interlaced)
			{
				env->ThrowError("InterlacedResize: Interlace not supported for YV12 yet");
			}
			DoYV12 = true;
			vOffsetsUV = (unsigned int*)_aligned_malloc(newheight * 4, 128);
			vWeightsUV = (unsigned int*)_aligned_malloc(newheight * 4, 128);

			if (!vOffsetsUV || !vWeightsUV)
			{
				env->ThrowError("SimpleResize: memory allocation error");
			}
		}

		else
		{
			env->ThrowError("SimpleResize: Horizontal pixels must divide by 2");
		}

		// 2 qwords, 2 offsets, and prefetch slack
		hControl = (unsigned int*)_aligned_malloc(newwidth * 12 + 128, 128);   // aligned for P4 cache line
        hControlUV = (unsigned int*)_aligned_malloc((newwidth / 2) * 12 + 128, 128);
		vWorkY = (unsigned int*)_aligned_malloc(2 * oldwidth + 128, 128);
		vWorkUV = (unsigned int*)_aligned_malloc(oldwidth + 128, 128);
		vOffsets = (unsigned int*)_aligned_malloc(newheight * 4, 128);
		vWeights = (unsigned int*)_aligned_malloc(newheight * 4, 128);

		if (!hControl || !vWeights)
		{
			env->ThrowError("SimpleResize: memory allocation error");
		}
		if (DoYV12)
		{
			InitTables_YV12();
		}
		else
		{
			InitTables();
		}
	}

	~SimpleResize()
	{
		_aligned_free(vOffsetsUV);
		_aligned_free(vWeightsUV);
		_aligned_free(hControl);
		_aligned_free(hControlUV);
		_aligned_free(vWorkY);
		_aligned_free(vWorkUV);
		_aligned_free(vOffsets);
		_aligned_free(vWeights);
	}
};


PVideoFrame __stdcall SimpleResize::GetFrame(int n, IScriptEnvironment* env)
{
	PVideoFrame dst;
	PVideoFrame src = child->GetFrame(n, env);
	if (has_at_least_v8) dst = env->NewVideoFrameP(vi, &src); else dst = env->NewVideoFrame(vi);

	if (DoYV12)
	{
		GetFrame_YV12(n, env, src, dst, PLANAR_Y, dst->GetWritePtr(PLANAR_Y));
		GetFrame_YV12(n, env, src, dst, PLANAR_U, dst->GetWritePtr(PLANAR_U));
		GetFrame_YV12(n, env, src, dst, PLANAR_V, dst->GetWritePtr(PLANAR_V));
	}
	else
	{
		GetFrame_YUY2(n, env, src, dst, PLANAR_Y, dst->GetWritePtr(PLANAR_Y));
	}
	return dst;
}

int SimpleResize::Fieldcopy(void* dest, const void* src, size_t count,
	int rows, int dst_pitch, int src_pitch)
{
	BYTE* pDest = (BYTE*)dest;
	BYTE* pSrc = (BYTE*)src;

	for (int i = 0; i < rows; i++)
	{
		memcpy(pDest, pSrc, count);
		pSrc += src_pitch;
		pDest += dst_pitch;
	}
	return 0;
}

int SimpleResize::FieldComp(void* dest, const void* src, size_t count,
	int rows, int dst_pitch, int src_pitch)
{
	BYTE* pDest = (BYTE*)dest;
	BYTE* pSrc = (BYTE*)src;
	int x = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < static_cast<int>(count); j++)
		{
			if (*(pSrc + j) != *(pDest + j))
			{
				x++;
			}
		}
		pSrc += src_pitch;
		pDest += dst_pitch;
	}
	return 0;
}

// YV12 Luma
PVideoFrame __stdcall SimpleResize::GetFrame_YV12(int n, IScriptEnvironment* env,
	PVideoFrame src, PVideoFrame dst, int Planar_Type, BYTE* dstp)
{
	int vWeight1[4];
	int vWeight2[4];
	const __int64 YMask[2] = { 0x00ff00ff00ff00ff,0x00ff00ff00ff00ff }; // keeps only luma
	const __int64 FPround1[2] = { 0x0080008000800080,0x0080008000800080 }; // round words
	const __int64 FPround2[2] = { 0x0000008000000080,0x0000008000000080 };// round dwords
	const __int64 FPround4 = 0x0080008000800080;// round words

	const BYTE* srcp = src->GetReadPtr(Planar_Type);
	const BYTE* srcp2W = srcp;
	BYTE* dstp2 = dstp;

	//	BYTE* dstp = dst->GetWritePtr(Planar_Type);
	const int src_pitch = src->GetPitch(Planar_Type);
	const int dst_pitch = dst->GetPitch(Planar_Type);
	const int src_row_size = src->GetRowSize(Planar_Type);
	const int row_size = dst->GetRowSize(Planar_Type);
	const int height = dst->GetHeight(Planar_Type);

	unsigned int* pControl = (Planar_Type == PLANAR_Y) ? &hControl[0] : &hControlUV[0];
	const unsigned char* srcp1;
	const unsigned char* srcp2;
	unsigned int* vWorkYW = vWorkY;

	unsigned int* vOffsetsW = (Planar_Type == PLANAR_Y)
		? vOffsets
		: vOffsetsUV;

	unsigned int* vWeightsW = (Planar_Type == PLANAR_Y)
		? vWeights
		: vWeightsUV;

	bool	SSE2enabledW = SSE2enabled;		// in local storage for asm
	bool	SSEMMXenabledW = SSEMMXenabled;		// in local storage for asm
//>>>>>>>> remove this 
//	bool	SSE2enabledW = 0;     SSE2enabled;		// in local storage for asm
//	bool	SSEMMXenabledW = SSEMMXenabled;		// in local storage for asm


	// Just in case things are not aligned right, maybe turn off sse2

	for (int y = 0; y < height; y++)
	{

		vWeight1[0] = vWeight1[1] = vWeight1[2] = vWeight1[3] =
			(256 - vWeightsW[y]) << 16 | (256 - vWeightsW[y]);
		vWeight2[0] = vWeight2[1] = vWeight2[2] = vWeight2[3] =
			vWeightsW[y] << 16 | vWeightsW[y];

		srcp1 = srcp + vOffsetsW[y] * src_pitch;

		if (Interlaced)
		{
			srcp2 = (y < height - 2)
				? srcp1 + 2 * src_pitch
				: srcp1;
		}
		else
		{
			srcp2 = (y < height - 1)
				? srcp1 + src_pitch
				: srcp1;
		}

		_asm
		{
			push	ecx						// have to save this?
			mov		ecx, src_row_size
			shr		ecx, 3					// 8 bytes a time
			mov		esi, srcp1				// top of 2 src lines to get
			mov		edx, srcp2				// next "
			mov		edi, vWorkYW			// luma work destination line
			xor eax, eax

			// Let's check here to see if we are on a P4 or higher and can use SSE2 instructions.
			// This first loop is not the performance bottleneck anyway but it is trivial to tune
			// using SSE2 if we have proper alignment.

			test    SSE2enabledW, 1			// is SSE2 supported?
			jz		vMaybeSSEMMX				// n, can't do anyway

			cmp     ecx, 2					// we have at least 16 byts, 2 qwords?
			jl		vMaybeSSEMMX				// n, don't bother

			mov		ebx, esi
			or ebx, edx
			test    ebx, 0xf				// both src rows 16 byte aligned?
			jnz		vMaybeSSEMMX			// n, don't use sse2

			shr		ecx, 1					// do 16 bytes at a time instead
			dec		ecx						// jigger loop ct
			align	16
			movdqu  xmm0, FPround1
			movdqu	xmm5, vWeight1
			movdqu	xmm6, vWeight2
			pxor	xmm7, xmm7

			align   16
			vLoopSSE2_Fetch:
			prefetcht0[esi + eax * 2 + 16]
				prefetcht0[edx + eax * 2 + 16]

				vLoopSSE2 :
				movdqu	xmm1, xmmword ptr[esi + eax] // top of 2 lines to interpolate
				movdqu	xmm3, xmmword ptr[edx + eax] // 2nd of 2 lines
				movdqa  xmm2, xmm1
				movdqa  xmm4, xmm3

				punpcklbw xmm1, xmm7			// make words
				punpckhbw xmm2, xmm7			// "
				punpcklbw xmm3, xmm7			// "
				punpckhbw xmm4, xmm7			// "

				pmullw	xmm1, xmm5				// mult by top weighting factor
				pmullw	xmm2, xmm5              // "
				pmullw	xmm3, xmm6				// mult by bot weighting factor
				pmullw	xmm4, xmm6              // "

				paddw	xmm1, xmm3				// combine lumas low
				paddw	xmm2, xmm4				// combine lumas high

				paddusw	xmm1, xmm0				// round
				paddusw	xmm2, xmm0				// round

				psrlw	xmm1, 8					// right adjust luma
				psrlw	xmm2, 8					// right adjust luma

				packuswb xmm1, xmm2				// pack words to our 16 byte answer
				movntdq	xmmword ptr[edi + eax], xmm1	// save lumas in our work area

				lea     eax, [eax + 16]
				dec		ecx						// don
				jg		vLoopSSE2_Fetch			// if not on last one loop, prefetch
				jz		vLoopSSE2				// or just loop, or not

	// done with our SSE2 fortified loop but we may need to pick up the spare change
				sfence
				mov		ecx, src_row_size		// get count again
				and ecx, 0x0000000f			// just need mod 16
				movq	mm5, vWeight1
				movq	mm6, vWeight2
				movq	mm0, FPround1			// useful rounding constant
				shr		ecx, 3					// 8 bytes at a time, any?
				jz		MoreSpareChange			// n, did them all		

	// Let's check here to see if we are on a P2 or Athlon and can use SSEMMX instructions.
	// This first loop is not the performance bottleneck anyway but it is trivial to tune
	// using SSE if we have proper alignment.
			vMaybeSSEMMX:
			movq	mm5, vWeight1
				movq	mm6, vWeight2
				movq	mm0, FPround1			// useful rounding constant
				pxor	mm7, mm7
				test    SSEMMXenabledW, 1		// is SSE supported?
				jz		vLoopMMX				// n, can't do anyway
				dec     ecx						// jigger loop ctr

				align	16
				vLoopSSEMMX_Fetch:
			prefetcht0[esi + eax + 8]
				prefetcht0[edx + eax + 8]

				vLoopSSEMMX :
				movq	mm1, qword ptr[esi + eax] // top of 2 lines to interpolate
				movq	mm3, qword ptr[edx + eax] // 2nd of 2 lines
				movq	mm2, mm1				// copy top bytes
				movq	mm4, mm3				// copy 2nd bytes

				punpcklbw mm1, mm7				// make words
				punpckhbw mm2, mm7				// "
				punpcklbw mm3, mm7				// "
				punpckhbw mm4, mm7				// "

				pmullw	mm1, mm5				// mult by weighting factor
				pmullw	mm2, mm5				// mult by weighting factor
				pmullw	mm3, mm6				// mult by weighting factor
				pmullw	mm4, mm6				// mult by weighting factor

				paddw	mm1, mm3				// combine lumas
				paddw	mm2, mm4				// combine lumas

				paddusw	mm1, mm0				// round
				paddusw	mm2, mm0				// round

				psrlw	mm1, 8					// right adjust luma
				psrlw	mm2, 8					// right adjust luma

				packuswb mm1, mm2				// pack UV's into low dword

				movntq	qword ptr[edi + eax], mm1	// save in our work area

				lea     eax, [eax + 8]
				dec		ecx
				jg		vLoopSSEMMX_Fetch			// if not on last one loop, prefetch
				jz		vLoopSSEMMX				// or just loop, or not
				sfence
				jmp		MoreSpareChange			// all done with vertical

				align	16
				vLoopMMX:
			movq	mm1, qword ptr[esi + eax] // top of 2 lines to interpolate
				movq	mm3, qword ptr[edx + eax] // 2nd of 2 lines
				movq	mm2, mm1				// copy top bytes
				movq	mm4, mm3				// copy 2nd bytes

				punpcklbw mm1, mm7				// make words
				punpckhbw mm2, mm7				// "
				punpcklbw mm3, mm7				// "
				punpckhbw mm4, mm7				// "

				pmullw	mm1, mm5				// mult by weighting factor
				pmullw	mm2, mm5				// mult by weighting factor
				pmullw	mm3, mm6				// mult by weighting factor
				pmullw	mm4, mm6				// mult by weighting factor

				paddw	mm1, mm3				// combine lumas
				paddw	mm2, mm4				// combine lumas

				paddusw	mm1, mm0				// round
				paddusw	mm2, mm0				// round

				psrlw	mm1, 8					// right just 
				psrlw	mm2, 8					// right just 

				packuswb mm1, mm2				// pack UV's into low dword

				movq	qword ptr[edi + eax], mm1	// save lumas in our work area

				lea     eax, [eax + 8]
				loop	vLoopMMX

				// Add a little code here to check if we have more pixels to do and, if so, make one
				// more pass thru vLoopMMX. We were processing in multiples of 8 pixels and alway have
				// an even number so there will never be more than 7 left. 
			MoreSpareChange:
			cmp		eax, src_row_size		// did we get them all
				jnl		DoHorizontal			// yes, else have 2 left
				mov		ecx, 1					// jigger loop ct
				mov		eax, src_row_size
				sub		eax, 8					// back up to last 8 pixels
				jmp		vLoopMMX


				// We've taken care of the vertical scaling, now do horizontal
			DoHorizontal:
			pxor    mm7, mm7
				movq	mm6, FPround2		// useful rounding constant, dwords
				mov		esi, pControl		// @ horiz control bytes			
				mov		ecx, row_size
				shr		ecx, 2				// 4 bytes a time, 4 pixels
				mov     edx, vWorkYW		// our luma data
				mov		edi, dstp			// the destination line
				test    SSEMMXenabledW, 1		// is SSE2 supported?
				jz		hLoopMMX				// n

	// With SSE support we will make 8 pixels (from 8 pairs) at a time
				shr		ecx, 1				// 8 bytes a time instead of 4
				jz		LessThan8
				align 16
				hLoopMMXSSE:
			// handle first 2 pixels			
			mov		eax, [esi + 16]		// get data offset in pixels, 1st pixel pair
				mov		ebx, [esi + 20]		// get data offset in pixels, 2nd pixel pair
				movd	mm0, [edx + eax]		// copy luma pair 0000xxYY
				punpcklwd mm0, [edx + ebx]    // 2nd luma pair, now xxxxYYYY
				punpcklbw mm0, mm7		    // make words out of bytes, 0Y0Y0Y0Y
				mov		eax, [esi + 16 + 24]	// get data offset in pixels, 3rd pixel pair
				mov		ebx, [esi + 20 + 24]	// get data offset in pixels, 4th pixel pair
				pmaddwd mm0, [esi]			// mult and sum lumas by ctl weights
				paddusw	mm0, mm6			// round
				psrlw	mm0, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// handle 3rd and 4th pixel pairs			
				movd	mm1, [edx + eax]		// copy luma pair 0000xxYY
				punpcklwd mm1, [edx + ebx]    // 2nd luma pair, now xxxxYYYY
				punpcklbw mm1, mm7		    // make words out of bytes, 0Y0Y0Y0Y
				mov		eax, [esi + 16 + 48]	// get data offset in pixels, 5th pixel pair
				mov		ebx, [esi + 20 + 48]	// get data offset in pixels, 6th pixel pair
				pmaddwd mm1, [esi + 24]		// mult and sum lumas by ctl weights
				paddusw	mm1, mm6			// round
				psrlw	mm1, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// handle 5th and 6th pixel pairs			
				movd	mm2, [edx + eax]		// copy luma pair 0000xxYY
				punpcklwd mm2, [edx + ebx]    // 2nd luma pair, now xxxxYYYY
				punpcklbw mm2, mm7		    // make words out of bytes, 0Y0Y0Y0Y
				mov		eax, [esi + 16 + 72]	// get data offset in pixels, 7th pixel pair
				mov		ebx, [esi + 20 + 72]	// get data offset in pixels, 8th pixel pair
				pmaddwd mm2, [esi + 48]			// mult and sum lumas by ctl weights
				paddusw	mm2, mm6			// round
				psrlw	mm2, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// handle 7th and 8th pixel pairs			
				movd	mm3, [edx + eax]		// copy luma pair
				punpcklwd mm3, [edx + ebx]    // 2nd luma pair
				punpcklbw mm3, mm7		    // make words out of bytes
				pmaddwd mm3, [esi + 72]		// mult and sum lumas by ctl weights
				paddusw	mm3, mm6			// round
				psrlw	mm3, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// combine, store, and loop
				packuswb mm0, mm1			// pack into qword, 0Y0Y0Y0Y
				packuswb mm2, mm3			// pack into qword, 0Y0Y0Y0Y
				packuswb mm0, mm2			// and again into  YYYYYYYY				
				movntq	qword ptr[edi], mm0	// done with 4 pixels

				lea    esi, [esi + 96]		// bump to next control bytest
				lea    edi, [edi + 8]			// bump to next output pixel addr
				dec	   ecx
				jg	   hLoopMMXSSE				// loop for more
				sfence

				LessThan8 :
			mov		ecx, row_size
				and ecx, 7				// we have done all but maybe this
				shr		ecx, 2				// now do only 4 bytes at a time
				jz		LessThan4

				align 16
				hLoopMMX:
			// handle first 2 pixels			
			mov		eax, [esi + 16]		// get data offset in pixels, 1st pixel pair
				mov		ebx, [esi + 20]		// get data offset in pixels, 2nd pixel pair
				movd	mm0, [edx + eax]		// copy luma pair 0000xxYY
				punpcklwd mm0, [edx + ebx]    // 2nd luma pair, now xxxxYYYY
				punpcklbw mm0, mm7		    // make words out of bytes, 0Y0Y0Y0Y
				mov		eax, [esi + 16 + 24]	// get data offset in pixels, 3rd pixel pair
				mov		ebx, [esi + 20 + 24]	// get data offset in pixels, 4th pixel pair
				pmaddwd mm0, [esi]			// mult and sum lumas by ctl weights
				paddusw	mm0, mm6			// round
				psrlw	mm0, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// handle 3rd and 4th pixel pairs			
				movd	mm1, [edx + eax]		// copy luma pair
				punpcklwd mm1, [edx + ebx]    // 2nd luma pair
				punpcklbw mm1, mm7		    // make words out of bytes
				pmaddwd mm1, [esi + 24]			// mult and sum lumas by ctl weights
				paddusw	mm1, mm6			// round
				psrlw	mm1, 8				// right just 4 luma pixel value 0Y0Y0Y0Y

				// combine, store, and loop
				packuswb mm0, mm1			// pack all into qword, 0Y0Y0Y0Y
				packuswb mm0, mm7			// and again into  0000YYYY				
				movd	dword ptr[edi], mm0	// done with 4 pixels
				lea    esi, [esi + 48]		// bump to next control bytest
				lea    edi, [edi + 4]			// bump to next output pixel addr
				loop   hLoopMMX				// loop for more

				// test to see if we have a mod 4 size row, if not then more spare change
				LessThan4 :
			mov		ecx, row_size
				and ecx, 3				// remainder size mod 4
				cmp		ecx, 2
				jl		LastOne				// none, done

				// handle 2 more pixels			
				mov		eax, [esi + 16]		// get data offset in pixels, 1st pixel pair
				mov		ebx, [esi + 20]		// get data offset in pixels, 2nd pixel pair
				movd	mm0, [edx + eax]		// copy luma pair 0000xxYY
				punpcklwd mm0, [edx + ebx]    // 2nd luma pair, now xxxxYYYY
				punpcklbw mm0, mm7		    // make words out of bytes, 0Y0Y0Y0Y

				pmaddwd mm0, [esi]			// mult and sum lumas by ctl weights
				paddusw	mm0, mm6			// round
				psrlw	mm0, 8				// right just 2 luma pixel value 000Y,000Y
				packuswb mm0, mm7			// pack all into qword, 00000Y0Y
				packuswb mm0, mm7			// and again into  000000YY		
				movd	dword ptr[edi], mm0	// store, we are guarrenteed room in buffer (8 byte mult)
				sub		ecx, 2
				lea		esi, [esi + 24]		// bump to next control bytest
				lea		edi, [edi + 2]			// bump to next output pixel addr

				// maybe one last pixel
				LastOne:
			cmp		ecx, 0				// still more?
				jz		AllDone				// n, done

				mov		eax, [esi + 16]		// get data offset in pixels, 1st pixel pair
				movd	mm0, [edx + eax]		// copy luma pair 0000xxYY
				punpcklbw mm0, mm7		    // make words out of bytes, xxxx0Y0Y

				pmaddwd mm0, [esi]			// mult and sum lumas by ctl weights
				paddusw	mm0, mm6			// round
				psrlw	mm0, 8				// right just 2 luma pixel value xxxx000Y
				movd	eax, mm0
				mov		byte ptr[edi], al	// store last one

				AllDone :
			pop		ecx
				emms
		}                               // done with one line
		dstp += dst_pitch;
	}

	return dst;
}


// YUY2
PVideoFrame __stdcall SimpleResize::GetFrame_YUY2(int n, IScriptEnvironment* env,
	PVideoFrame src, PVideoFrame dst, int Planar_Type, BYTE* dstp)
{
	int vWeight1[4];
	int vWeight2[4];
	//    unsigned char* dstp = dst->GetWritePtr(PLANAR_Y);

	const unsigned char* srcp = src->GetReadPtr(Planar_Type);

	// how do I do a __m128 constant, init and aligned on 16 byte boundar?
	const __int64 YMask[2] = { 0x00ff00ff00ff00ff,0x00ff00ff00ff00ff }; // keeps only luma
	const __int64 FPround1[2] = { 0x0080008000800080,0x0080008000800080 }; // round words
	const __int64 FPround2[2] = { 0x0000008000000080,0x0000008000000080 };// round dwords

	const int src_pitch = src->GetPitch();
	const int dst_pitch = dst->GetPitch();
	const int src_row_size = src->GetRowSize();
	const int row_size = dst->GetRowSize();
	const int height = dst->GetHeight();
	const unsigned int* pControl = &hControl[0];
	const unsigned char* srcp1;
	const unsigned char* srcp2;
	unsigned int* vWorkYW = vWorkY;
	unsigned int* vWorkUVW = vWorkUV;
	bool	SSE2enabledW = SSE2enabled;		// in local storage for asm
	bool	SSEMMXenabledW = SSEMMXenabled;		// in local storage for asm
	int EndOffset = src_row_size / 2;

	if (vi.IsYUY2())
	{
		for (int y = 0; y < height; y++)
		{

			vWeight1[0] = vWeight1[1] = vWeight1[2] = vWeight1[3] =
				(256 - vWeights[y]) << 16 | (256 - vWeights[y]);
			vWeight2[0] = vWeight2[1] = vWeight2[2] = vWeight2[3] =
				vWeights[y] << 16 | vWeights[y];

			srcp1 = srcp + vOffsets[y] * src_pitch;

			if (Interlaced)
			{
				srcp2 = (y < height - 2)
					? srcp1 + 2 * src_pitch
					: srcp1;
			}
			else
			{
				srcp2 = (y < height - 1)
					? srcp1 + src_pitch
					: srcp1;
			}

			_asm
			{
				push	ecx						// have to save this?
				mov		ecx, src_row_size
				shr		ecx, 3					// 8 bytes a time
				mov		esi, srcp1				// top of 2 src lines to get
				mov		edx, srcp2				// next "
				mov		edi, vWorkYW			// luma work destination line
				mov		ebx, vWorkUVW			// luma work destination line
				xor eax, eax

				// Let's check here to see if we are on a P4 or higher and can use SSE2 instructions.
				// This first loop is not the performance bottleneck anyway but it is trivial to tune
				// using SSE2 if we have proper alignment.

				test    SSE2enabledW, 1			// is SSE2 supported?
				jz		vMaybeSSEMMX				// n, can't do anyway
				cmp     ecx, 2					// we have at least 16 byts, 2 qwords?
				jl		vMaybeSSEMMX				// n, don't bother
				shr		ecx, 1					// do 16 bytes at a time instead
				dec		ecx						// jigger loop ct
				align	16
				movdqu  xmm0, FPround1
				movdqu	xmm5, vWeight1
				movdqu	xmm6, vWeight2
				movdqu	xmm7, YMask

				vLoopSSE2_Fetch :
				prefetcht0[esi + eax * 2 + 16]
					prefetcht0[edx + eax * 2 + 16]

					vLoopSSE2 :
					movdqu	xmm1, xmmword ptr[esi + eax * 2] // top of 2 lines to interpolate
					movdqu	xmm2, xmmword ptr[edx + eax * 2] // 2nd of 2 lines

					movdqa	xmm3, xmm1				// get chroma  bytes
					pand	xmm1, xmm7				// keep only luma
					psrlw	xmm3, 8					// right just chroma
					pmullw	xmm1, xmm5				// mult by weighting factor
					pmullw	xmm3, xmm5              // mult by weighting factor

					movdqa	xmm4, xmm2				// get chroma bytes
					pand	xmm2, xmm7				// keep only luma
					psrlw	xmm4, 8					// right just chroma
					pmullw	xmm2, xmm6				// mult by weighting factor
					pmullw	xmm4, xmm6              // mult by weighting factor

					paddw	xmm1, xmm2				// combine lumas
					paddusw	xmm1, xmm0				// round
					psrlw	xmm1, 8					// right adjust luma
					movntdq	xmmword ptr[edi + eax * 2], xmm1	// save lumas in our work area

					paddw	xmm3, xmm4				// combine chromas
					paddusw	xmm3, xmm0				// round
					psrlw	xmm3, 8					// right adjust chroma
					packuswb xmm3, xmm3				// pack UV's into low dword
					movdq2q	mm1, xmm3				// save in our work area
					movntq	qword ptr[ebx + eax], mm1 // save in our work area

					lea     eax, [eax + 8]
					dec		ecx						// don
					jg		vLoopSSE2_Fetch			// if not on last one loop, prefetch
					jz		vLoopSSE2				// or just loop, or not

		// done with our SSE2 fortified loop but we may need to pick up the spare change
					sfence
					mov		ecx, src_row_size		// get count again
					and ecx, 0x0000000f			// just need mod 16
					movq	mm7, YMask				// useful luma mask constant - lazy dupl init
					movq	mm5, vWeight1
					movq	mm6, vWeight2
					movq	mm0, FPround1			// useful rounding constant
					shr		ecx, 3					// 8 bytes at a time, any?
					jz		MoreSpareChange			// n, did them all		

		// Let's check here to see if we are on a P2 or Athlon and can use SSEMMX instructions.
		// This first loop is not the performance bottleneck anyway but it is trivial to tune
		// using SSE if we have proper alignment.
				vMaybeSSEMMX:
				movq	mm7, YMask				// useful luma mask constant
					movq	mm5, vWeight1
					movq	mm6, vWeight2
					movq	mm0, FPround1			// useful rounding constant
					test    SSEMMXenabledW, 1			// is SSE2 supported?
					jz		vLoopMMX				// n, can't do anyway
					dec     ecx						// jigger loop ctr

					align	16
					vLoopSSEMMX_Fetch:
				prefetcht0[esi + eax * 2 + 8]
					prefetcht0[edx + eax * 2 + 8]

					vLoopSSEMMX :
					movq	mm1, qword ptr[esi + eax * 2] // top of 2 lines to interpolate
					movq	mm2, qword ptr[edx + eax * 2] // 2nd of 2 lines

					movq	mm3, mm1				// copy top bytes
					pand	mm1, mm7				// keep only luma	
					pxor	mm3, mm1				// keep only chroma
					psrlw	mm3, 8					// right just chroma
					pmullw	mm1, mm5				// mult by weighting factor
					pmullw	mm3, mm5				// mult by weighting factor

					movq	mm4, mm2				// copy 2nd bytes
					pand	mm2, mm7				// keep only luma	
					pxor	mm4, mm2				// keep only chroma
					psrlw	mm4, 8					// right just chroma
					pmullw	mm2, mm6				// mult by weighting factor
					pmullw	mm4, mm6				// mult by weighting factor

					paddw	mm1, mm2				// combine lumas
					paddusw	mm1, mm0				// round
					psrlw	mm1, 8					// right adjust luma
					movntq	qword ptr[edi + eax * 2], mm1	// save lumas in our work area

					paddw	mm3, mm4				// combine chromas
					paddusw	mm3, mm0				// round
					psrlw	mm3, 8					// right adjust chroma
					packuswb mm3, mm3				// pack UV's into low dword
					movd	dword ptr[ebx + eax], mm3	// save in our work area

					lea     eax, [eax + 4]
					dec		ecx
					jg		vLoopSSEMMX_Fetch			// if not on last one loop, prefetch
					jz		vLoopSSEMMX				// or just loop, or not
					sfence
					jmp		MoreSpareChange			// all done with vertical

					align	16
					vLoopMMX:
				movq	mm1, qword ptr[esi + eax * 2] // top of 2 lines to interpolate
					movq	mm2, qword ptr[edx + eax * 2] // 2nd of 2 lines

					movq	mm3, mm1				// copy top bytes
					pand	mm1, mm7				// keep only luma	
					pxor	mm3, mm1				// keep only chroma
					psrlw	mm3, 8					// right just chroma
					pmullw	mm1, mm5				// mult by weighting factor
					pmullw	mm3, mm5				// mult by weighting factor

					movq	mm4, mm2				// copy 2nd bytes
					pand	mm2, mm7				// keep only luma	
					pxor	mm4, mm2				// keep only chroma
					psrlw	mm4, 8					// right just chroma
					pmullw	mm2, mm6				// mult by weighting factor
					pmullw	mm4, mm6				// mult by weighting factor

					paddw	mm1, mm2				// combine lumas
					paddusw	mm1, mm0				// round
					psrlw	mm1, 8					// right adjust luma
					movq	qword ptr[edi + eax * 2], mm1	// save lumas in our work area

					paddw	mm3, mm4				// combine chromas
					paddusw	mm3, mm0				// round
					psrlw	mm3, 8					// right adjust chroma
					packuswb mm3, mm3				// pack UV's into low dword
					movd	dword ptr[ebx + eax], mm3	// save in our work area

					lea     eax, [eax + 4]
					loop	vLoopMMX

					// Add a little code here to check if we have 2 more pixels to do and, if so, make one
					// more pass thru vLoopMMX. We were processing in multiples of 4 pixels and alway have
					// an even number so there will never be more than 2 left. trbarry 7/29/2002
				MoreSpareChange:
				cmp		eax, EndOffset			// did we get them all
					jnl		DoHorizontal			// yes, else have 2 left
					mov		ecx, 1					// jigger loop ct
					sub		eax, 2					// back up 2 pixels (4 bytes, but eax carried as 1/2)
					jmp		vLoopMMX


					// We've taken care of the vertical scaling, now do horizontal
				DoHorizontal:
				movq	mm7, YMask			// useful 0U0U..  mask constant
					movq	mm6, FPround2			// useful rounding constant, dwords
					mov		esi, pControl		// @ horiz control bytes			
					mov		ecx, row_size
					shr		ecx, 2				// 4 bytes a time, 2 pixels
					mov     edx, vWorkYW		// our luma data, as 0Y0Y 0Y0Y..
					mov		edi, dstp			// the destination line
					mov		ebx, vWorkUVW		// chroma data, as UVUV UVUV...
					align 16
					hLoopMMX:
				mov		eax, [esi + 16]		// get data offset in pixels, 1st pixel pair
					movd mm0, [edx + eax * 2]		// copy luma pair
					shr		eax, 1				// div offset by 2
					movd	mm1, [ebx + eax * 2]	// copy UV pair VUVU
					psllw   mm1, 8				// shift out V, keep 0000U0U0	

		// we need to use both even and odd croma from same location - trb 9/2002
					punpckldq mm1, [ebx + eax * 2]	// copy UV pair VUVU
					psrlw   mm1, 8				// shift out U0, keep 0V0V 0U0U	
		//

					mov		eax, [esi + 20]		// get data offset in pixels, 2nd pixel pair
					punpckldq mm0, [edx + eax * 2]		// copy luma pair
		/*
					shr		eax, 1				// div offset by 2
					punpckldq mm1, [ebx+eax*2]	// copy UV pair VUVU
					psrlw   mm1, 8				// shift out U0, keep 0V0V 0U0U
		*/
					pmaddwd mm0, [esi]			// mult and sum lumas by ctl weights
					paddusw	mm0, mm6			// round
					psrlw	mm0, 8				// right just 2 luma pixel value 000Y,000Y

					pmaddwd mm1, [esi + 8]		// mult and sum chromas by ctl weights
					paddusw	mm1, mm6			// round
					pslld	mm1, 8				// shift into low bytes of different words
					pand	mm1, mm7			// keep only 2 chroma values 0V00,0U00
					por		mm0, mm1			// combine luma and chroma, 0V0Y,0U0Y
					packuswb mm0, mm0			// pack all into low dword, xxxxVYUY
					movd	dword ptr[edi], mm0	// done with 2 pixels

					lea    esi, [esi + 24]		// bump to next control bytest
					lea    edi, [edi + 4]			// bump to next output pixel addr
					loop   hLoopMMX				// loop for more

					pop		ecx
					emms
			}                               // done with one line
			dstp += dst_pitch;
		}
	}

	else
	{			// RGB32
		env->ThrowError("SimpleResize: Supports YUY2 color format only");
	}
	return dst;

}



// This function accepts a position from 0 to 1 and warps it, to 0 through 1 based
// upon the wFact var. The warp equations are designed to:
// 
// * Always be rising but yield results from 0 to 1
//
// * Have a first derivative that doesn't go to 0 or infinity, at least close
//   to the center of the screen
//
// * Have a curvature (absolute val of 2nd derivative) that is small in the
//   center and smoothly rises towards the edges. We would like the curvature
//   to be everywhere = 0 when the warp factor = 1
//
double __stdcall SimpleResize::WarpFactor(double position, double wFact)
{
	double x;
	double z;
	double w;
	x = 2 * (position - .5);
	if (true) //(wFact < 1.0)
	// For warp factor < 1 the warp is calculated as (1-w) * x^3 + w *x, centered

	// The warp is calculated as z = (1 - w) * x^3 + w * x, centered
	// around .5 and ranging from 0 to 1. After some tinkering this seems
	// to give decent values and derivatives at the right places.
		w = 2.0 - wFact;	// reverse parm for compat with initial release

	if (x < 0.0)
	{
		z = -(1 - w) * x * x * x - w * x;      // -1 < x < 0, wFact < 1
		return .5 - .5 * z;
	}
	else
	{
		z = (1 - w) * x * x * x + w * x;      // -1 < x < 0, wFact < 1
		return .5 + .5 * z;				// amts to same formula as above for now
	}
}

// YUY2
// For each horizontal output pair of pixels there is are 2 qword masks followed by 2 int
// offsets. The 2 masks are the weights to be used for the luma and chroma, respectively.
// Each mask contains LeftWeight1, RightWeight1, LeftWeight2, RightWeight2. So a pair of pixels
// will later be processed each pass through the horizontal resize loop.

// The weights are scaled 0-256 and the left and right weights will sum to 256 for each pixel.


int __stdcall SimpleResize::InitTables(void)
{
	int i;
	int j;
	int k;
	int wY1;
	int wY2;
	int wUV1;
	int wUV2;

	// First set up horizontal table
	for (i = 0; i < newwidth; i += 2)
	{
		// first make even pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = i * 256 * (oldwidth - 1) / (newwidth - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>((256 * WarpFactor(i / (newwidth - 1.0), hWarp) * (oldwidth - static_cast<int64_t>(1))));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel
		wUV2 = (k % 2)
			? 128 + (wY2 >> 1)
			: wY2 >> 1;
		wUV1 = 256 - wUV2;

		if (k > oldwidth - 2)
		{
			hControl[i * 3 + 4] = oldwidth - 1;	 //	point to last byte
			hControl[i * 3] = 0x00000100;    // use 100% of rightmost Y
			hControl[i * 3 + 2] = 0x00000100;    // use 100% of rightmost U
		}
		else
		{
			hControl[i * 3 + 4] = k;			// pixel offset
			hControl[i * 3] = wY2 << 16 | wY1; // luma weights
			hControl[i * 3 + 2] = wUV2 << 16 | wUV1; // chroma weights
		}

		// now make odd pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = (i + 1) * 256 * (oldwidth - 1) / (newwidth - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>(256 * WarpFactor((i + static_cast<int64_t>(1)) / (newwidth - 1.0), hWarp) * (oldwidth - static_cast<int64_t>(1)));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel
		wUV2 = (k % 2)
			? 128 + (wY2 >> 1)
			: wY2 >> 1;
		wUV1 = 256 - wUV2;

		if (k > oldwidth - 2)
		{
			hControl[i * 3 + 5] = oldwidth - 1;	 //	point to last byte
			hControl[i * 3 + 1] = 0x00000100;    // use 100% of rightmost Y
			hControl[i * 3 + 3] = 0x00000100;    // use 100% of rightmost V
		}
		else
		{
			hControl[i * 3 + 5] = k;			// pixel offset
			hControl[i * 3 + 1] = wY2 << 16 | wY1; // luma weights

//			hControl[i*3+3] = wUV2 << 16 | wUV1; // chroma weights
// horiz chroma weights should be same as for even pixel - trbarry 09/16/2002
			hControl[i * 3 + 3] = hControl[i * 3 + 2]; // chroma weights 

		}
	}

	hControl[newwidth * 3 + 4] = 2 * (oldwidth - 1);		// give it something to prefetch at end
	hControl[newwidth * 3 + 5] = 2 * (oldwidth - 1);		// "

	// Next set up vertical table. The offsets are measured in lines and will be mult
	// by the source pitch later 
	for (i = 0; i < newheight; ++i)
	{
		if (vWarp == 1)			// if no warp factor
		{
			j = i * 256 * (oldheight - 1) / (newheight - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>(256 * WarpFactor(i / (newheight - 1.0), vWarp) * (oldheight - static_cast<int64_t>(1)));
		}
		if (Interlaced)						// do hard way?
		{
			if (i % 2)						// is odd output line?
			{
				if (j < 256)				// before 1st odd input line
				{
					vOffsets[i] = 1;		// all from line 1
					vWeights[i] = 0;		// weight to give to 2nd line
				}
				else
				{
					k = (((j - 256) >> 9) << 1) + 1; // next lowest odd line
					vOffsets[i] = k;
					wY2 = j - (k << 8);
					vWeights[i] = wY2 >> 1;	// weight to give to 2nd line
				}
			}
			else							// is even output line
			{
				k = (j >> 9) << 1;			// next lower even line
				vOffsets[i] = k;
				wY2 = j - (k << 8);
				vWeights[i] = wY2 >> 1;		// weight to give to 2nd line
			}
		}
		else								// simple way, do as progressive
		{
			k = j >> 8;
			vOffsets[i] = k;
			wY2 = j - (k << 8);
			vWeights[i] = wY2;				// weight to give to 2nd line
		}
	}

	return 0;
}

// YV12

// For each horizontal output pair of pixels there is are 2 qword masks followed by 2 int
// offsets. The 2 masks are the weights to be used for the luma and chroma, respectively.
// Each mask contains LeftWeight1, RightWeight1, LeftWeight2, RightWeight2. So a pair of pixels
// will later be processed each pass through the horizontal resize loop.  I think with my
// current math the Horizontal Luma and Chroma contains the same values but since I may have screwed it
// up I'll leave it this way for now. Vertical chroma is different.

// Note - try just using the luma calcs for both, seem to be the same.

// The weights are scaled 0-256 and the left and right weights will sum to 256 for each pixel.

int __stdcall SimpleResize::InitTables_YV12(void)
{
	int i;
	int j;
	int k;
	int wY1;
	int wY2;

	// First set up horizontal table, use for both luma & chroma since 
	// it seems to have the same equation.
	// We will geneerate these values in pairs, mostly because that's the way
	// I wrote it for YUY2 above.

	for (i = 0; i < newwidth; i += 2)
	{
		// first make even pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = i * 256 * (oldwidth - 1) / (newwidth - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>((256 * WarpFactor(i / (newwidth - 1.0), hWarp) * (oldwidth - static_cast<int64_t>(1))));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel

		if (k > oldwidth - 2)
		{
			hControl[i * 3 + 4] = oldwidth - 1;	 //	point to last byte
			hControl[i * 3] = 0x00000100;    // use 100% of rightmost Y
		}
		else
		{
			hControl[i * 3 + 4] = k;			// pixel offset
			hControl[i * 3] = wY2 << 16 | wY1; // luma weights
		}

		// now make odd pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = (i + 1) * 256 * (oldwidth - 1) / (newwidth - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>((256 * WarpFactor((i + static_cast<int64_t>(1)) / (newwidth - 1.0), hWarp) * (oldwidth - static_cast<int64_t>(1))));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel

		if (k > oldwidth - 2)
		{
			hControl[i * 3 + 5] = oldwidth - 1;	 //	point to last byte
			hControl[i * 3 + 1] = 0x00000100;    // use 100% of rightmost Y
		}
		else
		{
			hControl[i * 3 + 5] = k;			// pixel offset
			hControl[i * 3 + 1] = wY2 << 16 | wY1; // luma weights
		}
	}

	hControl[newwidth * 3 + 4] = 2 * (oldwidth - 1);		// give it something to prefetch at end
	hControl[newwidth * 3 + 5] = 2 * (oldwidth - 1);		// "
	hControl[newwidth * 3 + 4] = 2 * (oldwidth - 1);		// give it something to prefetch at end
	hControl[newwidth * 3 + 5] = 2 * (oldwidth - 1);		// "

	for (i = 0; i < newwidth / 2; i+= 2)
	{
		// first make even pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = static_cast<int>(i * static_cast<int64_t>(256) * (oldwidth / 2 - static_cast<int64_t>(1)) / (newwidth / 2 - 1.0));
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>((256 * WarpFactor(i / (newwidth / 2 - 1.0), hWarp) * (oldwidth / 2 - 1.0)));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel

		if (k > oldwidth / 2 - 2)
		{
			hControlUV[i * 3 + 4] = oldwidth / 2 - 1;	 //	point to last byte
			hControlUV[i * 3] = 0x00000100;    // use 100% of rightmost Y
		}
		else
		{
			hControlUV[i * 3 + 4] = k;			// pixel offset
			hControlUV[i * 3] = wY2 << 16 | wY1; // luma weights
		}

		// now make odd pixel control
		if (hWarp == 1)			// if no warp factor
		{
			j = static_cast<int>((i + 1.0) * 256 * (oldwidth / 2 - 1.0) / (newwidth / 2 - 1.0));
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>(256 * WarpFactor((i + 1.0) / (newwidth / 2 - 1.0), hWarp) * (oldwidth / 2 - 1.0));
		}

		k = j >> 8;
		wY2 = j - (k << 8);				// luma weight of right pixel
		wY1 = 256 - wY2;				// luma weight of left pixel

		if (k > oldwidth / 2 - 2)
		{
			hControlUV[i * 3 + 5] = oldwidth / 2 - 1;	 //	point to last byte
			hControlUV[i * 3 + 1] = 0x00000100;    // use 100% of rightmost Y
		}
		else
		{
			hControlUV[i * 3 + 5] = k;			// pixel offset
			hControlUV[i * 3 + 1] = wY2 << 16 | wY1; // luma weights
		}
	}

	hControlUV[(newwidth / 2) * 3 + 4] = 2 * (oldwidth / 2 - 1);		// give it something to prefetch at end
	hControlUV[(newwidth / 2) * 3 + 5] = 2 * (oldwidth / 2 - 1);		// "
	hControlUV[(newwidth / 2) * 3 + 4] = 2 * (oldwidth / 2 - 1);		// give it something to prefetch at end
	hControlUV[(newwidth / 2) * 3 + 5] = 2 * (oldwidth / 2 - 1);		// "

	// Next set up vertical tables. The offsets are measured in lines and will be mult
	// by the source pitch later .

	// For YV12 we need separate Luma and chroma tables
	// First Luma Table

	for (i = 0; i < newheight; ++i)
	{
		if (vWarp == 1)			// if no warp factor
		{
			j = i * 256 * (oldheight - 1) / (newheight - 1);
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>(256 * WarpFactor(i / (newheight - 1.0), vWarp) * (oldheight - static_cast<int64_t>(1)));
		}
		if (Interlaced)						// do hard way?
		{
			if (i % 2)						// is odd output line?
			{
				if (j < 256)				// before 1st odd input line
				{
					vOffsets[i] = 1;		// all from line 1
					vWeights[i] = 0;		// weight to give to 2nd line
				}
				else
				{
					k = (((j - 256) >> 9) << 1) + 1; // next lowest odd line
					vOffsets[i] = k;
					wY2 = j - (k << 8);
					vWeights[i] = wY2 >> 1;	// weight to give to 2nd line
				}
			}
			else							// is even output line
			{
				k = (j >> 9) << 1;			// next lower even line
				vOffsets[i] = k;
				wY2 = j - (k << 8);
				vWeights[i] = wY2 >> 1;		// weight to give to 2nd line
			}
		}
		else								// simple way, do as progressive
		{
			k = j >> 8;
			vOffsets[i] = k;
			wY2 = j - (k << 8);
			vWeights[i] = wY2;				// weight to give to 2nd line
		}
	}

	// Vertical table for chroma
	for (i = 0; i < newheight / 2; ++i)
	{
		if (vWarp == 1)			// if no warp factor
		{
			j = static_cast<int>(i * static_cast<int64_t>(256) * (oldheight / 2 - static_cast<int64_t>(1)) / (newheight / 2 - 1.0));
		}
		else					// stretch and warp somehow
		{
			j = static_cast<int>(256 * WarpFactor(i / (newheight / 2 - 1.0), vWarp) * (oldheight / 2 - 1.0));
		}
		if (Interlaced)						// do hard way?
		{
			if (i % 2)						// is odd output line?
			{
				if (j < 256)				// before 1st odd input line
				{
					vOffsetsUV[i] = 1;		// all from line 1
					vWeightsUV[i] = 0;		// weight to give to 2nd line
				}
				else
				{
					k = (((j - 256) >> 9) << 1) + 1; // next lowest odd line
					vOffsetsUV[i] = k;
					wY2 = j - (k << 8);
					vWeightsUV[i] = wY2 >> 1;	// weight to give to 2nd line
				}
			}
			else							// is even output line
			{
				k = (j >> 9) << 1;			// next lower even line
				vOffsetsUV[i] = k;
				wY2 = j - (k << 8);
				vWeightsUV[i] = wY2 >> 1;		// weight to give to 2nd line
			}
		}
		else								// simple way, do as progressive
		{
			k = j >> 8;
			vOffsetsUV[i] = k;
			wY2 = j - (k << 8);
			vWeightsUV[i] = wY2;				// weight to give to 2nd line
		}
	}

	return 0;
}

AVSValue __cdecl Create_InterlacedResize(AVSValue args, void* user_data, IScriptEnvironment* env)
{
	_asm emms		// don't trust environment
	return new SimpleResize(args[0].AsClip(), args[1].AsInt(), args[2].AsInt(),
		1.0, 1.0, true, env);
}

AVSValue __cdecl Create_InterlacedWarpedResize(AVSValue args, void* user_data, IScriptEnvironment* env)
{
	_asm emms		// don't trust environment
	return new SimpleResize(args[0].AsClip(), args[1].AsInt(), args[2].AsInt(),
		args[3].AsFloat(), args[4].AsFloat(), true, env);
}


AVSValue __cdecl Create_SimpleResize(AVSValue args, void* user_data, IScriptEnvironment* env)
{
	_asm emms		// don't trust environment
	return new SimpleResize(args[0].AsClip(), args[1].AsInt(), args[2].AsInt(),
		1.0, 1.0, false, env);
}

AVSValue __cdecl Create_WarpedResize(AVSValue args, void* user_data, IScriptEnvironment* env)
{
	_asm emms		// don't trust environment
	return new SimpleResize(args[0].AsClip(), args[1].AsInt(), args[2].AsInt(),
		args[3].AsFloat(), args[4].AsFloat(), false, env);
}

const AVS_Linkage* AVS_linkage;

extern "C" __declspec(dllexport)
const char* __stdcall AvisynthPluginInit3(IScriptEnvironment * env, const AVS_Linkage* const vectors)
{
	AVS_linkage = vectors;

	env->AddFunction("SimpleResize", "cii", Create_SimpleResize, 0);
	env->AddFunction("WarpedResize", "ciiff", Create_WarpedResize, 0);
	env->AddFunction("InterlacedResize", "cii", Create_InterlacedResize, 0);
	env->AddFunction("InterlacedWarpedResize", "ciiff", Create_InterlacedWarpedResize, 0);
	return "`SimpleResize' Fast Resizer";
}
