Changes by Asd-g
2020-05-18
http://avisynth.nl/index.php/SimpleResize

* Update to AviSynth+'s v8 interface
* x64 version: used the original source code and just replaced 32-bit registers 
  with 64-bit registers
*  Compiled with MSVC 2019 (Clang 10), x86 and x64 build included

2020-06-17

* Fixed memory leak
* Fixed WarpedResize YV12 processing